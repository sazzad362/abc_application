
$(function() {
    "use strict";
     
	 
//sidebar menu js
$.sidebarMenu($('.sidebar-menu'));

// === toggle-menu js

$(".toggle-menu").on("click", function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });	   
	
	   
// === sidebar menu activation js

$(function() {
        for (var i = window.location, o = $(".sidebar-menu a").filter(function() {
            return this.href == i;
        }).addClass("active").parent().addClass("active"); ;) {
            if (!o.is("li")) break;
            o = o.parent().addClass("in").parent().addClass("active");
        }
    }), 	   
	   
/* Back To Top */

$(document).ready(function(){ 
    $(window).on("scroll", function(){ 
        if ($(this).scrollTop() > 300) { 
            $('.back-to-top').fadeIn(); 
        } else { 
            $('.back-to-top').fadeOut(); 
        } 
    }); 
    $('.back-to-top').on("click", function(){ 
        $("html, body").animate({ scrollTop: 0 }, 600); 
        return false; 
    }); 
});	   
	   
$(function () {
  $('[data-toggle="popover"]').popover()
})


$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

});
// Calculate sales from
$('#sale_saleId, #sale_paidId').on('input',function() {

    var sale_price = parseFloat($('#sale_saleId').val());
    var sale_paidId = parseFloat($('#sale_paidId').val());

    $('#sales_due').val((sale_price - sale_paidId));
});

/* =====================
    Invoice Canculation
=========================*/
 $(document).ready(function(){
    var i=1;
    $("#add_row").click(function(){b=i-1;
        $('#addr'+i).html($('#addr'+b).html());
        $('#tab_logic').append('<tr id="addr'+(i+1)+'"></tr>');
        i++; 
    });
    $("#delete_row").click(function(){
      if(i>1){
    $("#addr"+(i-1)).html('');
    i--;
    }
    calc();
  });
  
  $('#tab_logic tbody').on('keyup change',function(){
    calc();
  });

  $('#fare', '#intensive', '#paid', '#pre_due').on('keyup change',function(){
    calc_total();
  });
  

});

function calc()
{
  $('#tab_logic tbody tr').each(function(i, element) {
    var html = $(this).html();
    if(html!='')
    {
      var ctn = $(this).find('.ctn').val();
      var qty = $(this).find('.qty').val();
      var rate = $(this).find('.rate').val();

      var quantity_price = ctn * qty * rate

      $(this).find('.price').val(quantity_price.toFixed(2));

      // Sub Field   
      
      calc_total();
    }

    });
}

function calc_total()
{
  total=0;

  $('.price').each(function() {
        total += parseInt($(this).val());
    });

  $('#sub_total').val(total.toFixed(2));
    var fare = $('#fare').val();
    var intensive = $('#intensive').val();
    var paid = $('#paid').val();
    var pre_due = $('#pre_due').val();   
    var sdTotal = Number(total) + Number(pre_due);


    $('.grandTotal').val(sdTotal.toFixed(2));

    var total_fare = Number(sdTotal) - Number(fare);

    var total_intensive = Number(total_fare) - Number(intensive);

    //Due 

    var totalDue = Number(total_intensive) - Number(paid);

    $('.due').val(totalDue.toFixed(2));
  
}


/* Back To Top */




