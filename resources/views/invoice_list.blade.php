@extends('layouts.template')







@section('content')







<div class="content-wrapper">



    <div class="container-fluid">







        <!--Start Dashboard Content-->



        @if (session('invoice_delete'))



            <div class="alert alert-success p-3" role="alert">



                {{ session('invoice_delete') }}



            </div>



        @endif







        



        <!-- ==========================



            All Invoice List



        =============================-->



      



      <div class="row">



        <div class="col-lg-12 mt-3">



          <div class="card">



            <div class="card-body">



              <h5 class="card-title">All Invoice</h5>



               <div class="table-responsive">



                <table class="table table-bordered text-center">



                  <thead>



                    <tr>



                      <th scope="col">SL NO</th>



                      <th scope="col">Customer Name</th>



                      <th scope="col">Invoce ID</th>

                      <th scope="col">Date</th>



                      <th scope="col">Action</th>



                    </tr>



                  </thead>



                  <tbody>



                    <?php 



                      $i = 0;



                      foreach ($invoice_list as $item) { 



                        $i++;



                    ?>



                    <tr>



                      <th scope="row">{{ $i }}</th>



                      <td>{{ $item->name }}</td>



                      <td>{{ $item->id }}</td>



                      <td>

                        <?php 

                        $originalDate = $item->created_at;

                        echo $newDate = date("d-m-Y", strtotime($originalDate));

                        ?>

                      </td>



                      <td>



                        <a href="{{ URL::to('invoice_prient/'.$item->id) }}" target="_blank" class="btn btn-success waves-effect waves-light btn-sm"><i class="fa fa-print"></i> Print</a>



                        <a href="{{ URL::to('invoice_pdf/'.$item->id) }}" target="_blank" class="btn btn-success waves-effect waves-light btn-sm"><i class="fa fa-file-pdf-o"></i> PDF</a>

                        <a href="{{ URL::to('invoice_delete/'.$item->id) }}" class="btn btn-danger waves-effect waves-light btn-sm"><i class="fa fa-trash-o"></i></a>



                      </td>



                    </tr>



                    <?php } ?>







                  </tbody>



                </table>



               </div>



            </div>



          </div>



        </div>











      </div> <!-- End Row -->







        







    </div><!-- End container-fluid-->



    



</div> <!-- End Content waper -->



@endsection