@extends('layouts.template')

@section('content')

<div class="content-wrapper">
    <div class="container-fluid">

        <!--Start Dashboard Content-->
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        @if( session('customer_delete') )
          <div class="alert alert-success p-4" role="alert">
            {{ Session::get('customer_delete') }}
          </div>
        @endif

        @if( session('customer_update') )
          <div class="alert alert-success p-4" role="alert">
            {{ Session::get('customer_update') }}
          </div>
        @endif

        <!-- ==========================
            All Customer Customet Form
        =============================-->
      
      <div class="row">
        <div class="col-lg-12 mt-3">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Customer Search</h5>
               <div class="table-responsive">
                <table class="table table-bordered text-center">
                  <thead>
                    <tr>
                      <th scope="col">SL NO</th>
                      <th scope="col">Name</th>
                      <th scope="col">Address</th>
                      <th scope="col">Phone</th>
                      <th scope="col">Pre. Due</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                      
                    <?php 
                       $i = 1;
                     ?>

                    <tr>
                      <th scope="row"> <?php echo $i++; ?> </th>
                      <td>{{ $dashboard_search->name }}</td>
                      <td>{{ $dashboard_search->address }}</td>
                      <td>{{ $dashboard_search->phone }}</td>
                      <td>
                        <?php 
                          if ($NewPreDue != '') {
                            echo $NewPreDue;
                          }else{
                            echo "N/A";
                          }
                        ?>
                      </td>
                      <td>
                        <a href="{{ URL::to("/customer_edit/".$dashboard_search->id )}}" class="btn btn-success waves-effect waves-light btn-sm"><i class="fa fa-pencil"></i></a>
                      </td>
                    </tr>

                  </tbody>
                </table>
               </div>
            </div>
          </div>
        </div>

      </div> <!-- End Row -->

    </div><!-- End container-fluid-->
    
</div> <!-- End Content waper -->
@endsection