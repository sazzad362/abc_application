<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Print Data</title>
  <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet"/>
  <link href="{{ asset('assets/css/icons.css') }}" rel="stylesheet" type="text/css"/>
  <style>
    .print_margin{
      margin-top: 30px;
    }
    .table td, .table th {
      padding: 7px 20px;
      vertical-align: top;
      border-top: 1px solid #dee2e6;
  }
  .header_title h5 {
    padding: 10px;
    background: #ddd;
    color: #333;
    text-align: center;
}
@media print {
  #printPageButton {
    display: none;
  }
}
  </style>
</head>
<body>

  <div class="container print_margin">
    <div class="header_title">
      <h5>Ladger Sheet For :
        <span>
          <?php 
            $i = 0;
            foreach ($statements as $item) {
             $i++;
             $name = $item->name;
             $address = $item->address;
             if ($i <= 1){
              echo  $name .' ' ."(" .$address .")";
             }
            }
          ?>
        </span> <br>
        Date:
       <span class="text-danger"><?php echo $newStartDate = date("d-m-Y", strtotime($date_start)); ?></span> TO <span class="text-danger"><?php echo $newEndDate = date("d-m-Y", strtotime($date_end)); ?></span>
     </h5>

    </div>
  <table class="table_area table table-bordered">
    <thead class="table_head">
      <!-- =======================
              Table Header ======= -->
      <tr>
        <th scope="col">SL NO</th>
        <th scope="col">Date</th>
        <th scope="col">Sales Ctn.</th>
        <th scope="col">Sales Amount</th>
        <th scope="col">Payment</th>
        <th scope="col">Advance</th>
        <th scope="col">Due</th>
      </tr>
    </thead>
    <tbody class="table_body">
      <!-- ================
            Table Content = -->
      <?php $i = 0; ?>
     @foreach( $statements as $item )
      <?php $i++; ?>
        <tr>
          <td scope="row">{{ $i }}</td>
          <td>{{ $item->date }} </td>
          <td>
            {{ $item->sales_ctn }}
          </td>
          <td>{{ $item->sales_amount }}</td>
          <td>
            {{ $item->paid_amount }}
          </td>

          <?php $NewBlance = $item->sales_amount - $item->paid_amount  ?> 

          <td>
            <?php 
              if ($NewBlance < 0) {
                echo abs($NewBlance);
              }else{
                echo "N/A";
              }
            ?>
          </td>

          <td class="text-danger">

            <?php 
              if ($NewBlance > 0) {
                echo abs($NewBlance);
              }else{
                echo "N/A";
              }
             ?>

          </td>

        </tr>
      @endforeach

    </tbody>
</table>


<div class="btn-group" role="group" aria-label="Basic example">
 <a href="javascript:window.print();" target="_blank" class="btn btn-outline-secondary m-1" id="printPageButton"><i class="fa fa-print"></i> Print</a>
  <a href="/ledger_customer" class="btn btn-outline-secondary m-1" id="printPageButton"><i class="fa fa-search"></i> Search Again</a>
</div>

</div>

</body>
</html>