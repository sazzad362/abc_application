@extends('layouts.template')

@section('content')

<div class="content-wrapper">
    <div class="container-fluid">

        <!--Start Dashboard Content-->
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        @if( session('specification_create') )
          <div class="alert alert-success p-4" role="alert">
            {{ Session::get('specification_create') }}
          </div>
        @endif
        
        @if( session('specification_update') )
          <div class="alert alert-success p-4" role="alert">
            {{ Session::get('specification_update') }}
          </div>
        @endif

        @if( session('specification_delete') )
          <div class="alert alert-success p-4" role="alert">
            {{ Session::get('specification_delete') }}
          </div>
        @endif

      <!-- ==========================
            Specification ADD
        =============================-->

      <div class="row">
        <div class="col-lg-12">
           <div class="card">
             <div class="card-body">
               <div class="card-title">Add Product Specification</div>
               <hr>
                <form method="POST" action="{{ URL::to("/update_specification/".$specification_edit->id)}}">
                {{ csrf_field() }}
               <div class="form-group">
                <label for="input-1">Product Specification</label>
                <input type="text" class="form-control" id="input-1" name="specification_name"  value="{{ $specification_edit->specification_name }}" required="TRUE" autocomplete="OFF">
               </div>
               <div class="form-group">
                <button type="submit" class="btn btn-primary shadow-primary px-5"><i class="icon-lock"></i> Submit</button>
              </div>
              </form>
             </div>
           </div>
        </div> <!-- End Col 12 -->

      </div><!--End Row-->

    </div><!-- End container-fluid-->
    
</div> <!-- End Content waper -->
@endsection