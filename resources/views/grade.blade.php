@extends('layouts.template')

@section('content')

<div class="content-wrapper">
    <div class="container-fluid">

        <!--Start Dashboard Content-->
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        @if( session('specification_create') )
          <div class="alert alert-success p-4" role="alert">
            {{ Session::get('specification_create') }}
          </div>
        @endif
        
        @if( session('specification_update') )
          <div class="alert alert-success p-4" role="alert">
            {{ Session::get('specification_update') }}
          </div>
        @endif

        @if( session('specification_delete') )
          <div class="alert alert-success p-4" role="alert">
            {{ Session::get('specification_delete') }}
          </div>
        @endif

      <!-- ==========================
            Product Grade
        =============================-->

      <div class="row">
        <div class="col-lg-12">
           <div class="card">
             <div class="card-body">
               <div class="card-title">Add Product Grade</div>
               <hr>
                <form method="POST" action="{{ URL::to("/create_grade")}}">
                {{ csrf_field() }}
               <div class="form-group">
                <label for="input-1">Product Grade</label>
                <input type="text" class="form-control" id="input-1" name="grade_name" placeholder="Enter Product Grade. Ex: A" required="TRUE" autocomplete="OFF">
               </div>
               <div class="form-group">
                <button type="submit" class="btn btn-primary shadow-primary px-5"><i class="icon-lock"></i> Submit</button>
              </div>
              </form>
             </div>
           </div>
        </div> <!-- End Col 12 -->

        <div class="col-lg-12 mt-3">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Grade List</h5>
               <div class="table-responsive">
                <table class="table table-bordered text-center">
                  <thead>
                    <tr>
                      <th scope="col">SL NO</th>
                      <th scope="col">Grade</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                       $i = 1;
                     ?>
                     @foreach( $grade as $item )
                    <tr>
                      <th scope="row"><?php echo $i++; ?></th>
                      <td>Grade : {{ $item->grade_name }}</td>
                      <td>
                        <a href="{{ URL::to("/grade_edit/".$item->id )}}" class="btn btn-success waves-effect waves-light btn-sm"><i class="fa fa-pencil"></i></a>
                        <a href="{{ URL::to("/grade_delete/".$item->id )}}" class="btn btn-danger waves-effect waves-light btn-sm"><i class="fa fa-trash-o"></i></a>
                      </td>
                    </tr>
                     @endforeach

                  </tbody>
                </table>
               </div>
            </div>
          </div>
        </div>

      </div><!--End Row-->

    </div><!-- End container-fluid-->
    
</div> <!-- End Content waper -->
@endsection