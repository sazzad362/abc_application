<!doctype html>

<html lang="{{ app()->getLocale() }}">

    <head>

        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="viewport" content="width=device-width, initial-scale=1">



        <title>AB Ceramics Industries</title>



        <!-- Fonts -->

        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">



        <!-- Styles -->

        <style>

            html, body {

                background-color: #fff;

                color: #636b6f;

                font-family: 'Nunito', sans-serif;

                font-weight: 200;

                height: 100vh;

                margin: 0;

            }



            .full-height {

                height: 100vh;

            }



            .flex-center {

                align-items: center;

                display: flex;

                justify-content: center;

            }



            .position-ref {

                position: relative;

            }



            .top-right {

                position: absolute;

                right: 10px;

                top: 18px;

                z-index: 1;

            }



            .content {

                text-align: center;
                z-index: 1;

            }



            .title {

                font-size: 84px;

            }



            .links > a {

                color: #fff;

                padding: 0 25px;

                font-size: 12px;

                font-weight: 600;

                letter-spacing: .1rem;

                text-decoration: none;

                text-transform: uppercase;

            }



            .m-b-md {

                margin-bottom: 30px;

            }

            .bodybg{
                background-image: url(assets/images/bg.jpg);
                width: 100%;
                height: 100%;
                color:#fff;
                position: relative;
            }
            .bodybg:after{
                content: '';
                width: 100%;
                height: 100%;
                background-color: #000000db;
                position: absolute;
                top: 0;
                left: 0;
            }

        </style>

    </head>

    <body>

        <div class="flex-center position-ref full-height bodybg">

            @if (Route::has('login'))

                <div class="top-right links">

                    @auth

                        <a href="{{ url('/home') }}">Home</a>

                    @else

                        <a href="{{ route('login') }}">Login</a>

                    @endauth

                </div>

            @endif



            <div class="content">

                <h1>Welcome to AB Ceramics Industries</h1>
                <h3>We are coming soon!</h3>

            </div>

        </div>

    </body>

</html>

