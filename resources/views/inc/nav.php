            <ul class="sidebar-menu do-nicescrol">
                <li class="sidebar-header">MAIN NAVIGATION</li>
                <li>
                    <a href="/home" class="waves-effect">
                        <i class="icon-home"></i><span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="javaScript:void();" class="waves-effect">
                        <i class="icon-people icons"></i><span>Customer Manage</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="sidebar-submenu">
                        <li><a href="/add_customer"><i class="fa fa-long-arrow-right"></i> Add New Customer</a></li>
                        <li><a href="/customer_list"><i class="fa fa-long-arrow-right"></i> All Customer</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javaScript:void();" class="waves-effect">
                        <i class="icon-handbag icons"></i><span>Product Meta</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="sidebar-submenu">
                        <li><a href="grade"><i class="fa fa-long-arrow-right"></i> Product Grade</a></li>
                        <li><a href="/specification"><i class="fa fa-long-arrow-right"></i> Specification</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javaScript:void();" class="waves-effect">
                        <i class="icon-target icons"></i><span>Product Manage</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="sidebar-submenu">
                        <li><a href="/add_product"><i class="fa fa-long-arrow-right"></i> Add New Product</a></li>
                        <li><a href="/all_product"><i class="fa fa-long-arrow-right"></i> All Product</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javaScript:void();" class="waves-effect">
                        <i class="icon-basket-loaded icons"></i><span>Sales Manage</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="sidebar-submenu">
                        <li><a href="/add_sales"><i class="fa fa-long-arrow-right"></i> Add New Sales</a></li>
                        <li><a href="/all_sales"><i class="fa fa-long-arrow-right"></i> All Sales</a></li>
                        <li><a href="/sale_search"><i class="fa fa-long-arrow-right"></i> Search Sales</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javaScript:void();" class="waves-effect">
                        <i class="icon-chart icons"></i><span>Ledger Manage</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="sidebar-submenu">
                        <li><a href="/ledger_daily"><i class="fa fa-long-arrow-right"></i> Daily Ledger</a></li>
                        <li><a href="/ledger_monthly"><i class="fa fa-long-arrow-right"></i> Monthly Ledger</a></li>
                        <li><a href="/ledger_customer"><i class="fa fa-long-arrow-right"></i>Customer Statements</a></li>
                    </ul>
                </li>
                <li>
                    <a href="javaScript:void();" class="waves-effect">
                        <i class="icon-docs icons"></i><span>Invoice</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="sidebar-submenu">
                        <li><a href="/invoice"><i class="fa fa-long-arrow-right"></i> Add New Invoice</a></li>
                        <li><a href="/invoice_list"><i class="fa fa-long-arrow-right"></i> Invoice List</a></li>
                        <li><a href="/invoice_search"><i class="fa fa-long-arrow-right"></i> Invoice Search</a></li>
                    </ul>
                </li>
            </ul>