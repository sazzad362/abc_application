<?php
  use App\DeuPayment;
?>
@extends('layouts.template')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">

        <!--Start Dashboard Content-->
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        @if( session('sales_create') )
          <div class="alert alert-success p-4" role="alert">
            {{ Session::get('sales_create') }}
          </div>
        @endif

        @if( session('sales_delete') )
          <div class="alert alert-success p-4" role="alert">
            {{ Session::get('sales_delete') }}
          </div>
        @endif

        @if( session('sales_update') )
          <div class="alert alert-success p-4" role="alert">
            {{ Session::get('sales_update') }}
          </div>
        @endif

        <!-- ==========================
            List Of All Sales
        =============================-->

      <div class="row">
        <div class="col-lg-12 mt-3">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Sales Search Result For <span class="text-danger"><?php echo $newStartDate = date("d-m-Y", strtotime($date_start)); ?></span></h5>
               <div class="table-responsive">
                <table class="table table-bordered text-center">
                  <thead>
                    <!-- =======================
                            Table Header ======= -->
                    <tr>
                      <th scope="col">SL NO</th>
                      <th scope="col">Customer Name</th>
                      <th scope="col">Sale Amount</th>
                      <th scope="col">Paid</th>
                      <th scope="col">Adv. / Due</th>
                      <th scope="col">Date</th>
                      <th scope="col">Action</th>
                    </tr>

                  </thead>
                  <tbody>
                    <!-- ================
                          Table Content = -->
                   <?php 
                    $i = 1;
                   ?>
                    @foreach( $sales_search as $item )
                    <tr>
                      <th scope="row"> {{ $i++ }} </th>
                      <th>{{ $item->name }} ({{ $item->address }})</th>
                      <td>{{ $item->sales_amount }}</td>
                      <td>{{ $item->paid_amount }}</td>
                      <td class="text-danger">
                        <?php 
                          $due = $item->due_amount;
                          $due_payment = DeuPayment::where('customer_id', $item->customner_id)
                                        ->where('sales_manage_id', $item->id)
                                        ->get();
                          
                          
                          foreach ($due_payment as $items) {
                              $total_due = $items->due_payment;
                            echo $total_due;
                          }
                          
                          // $adv = abs($due);
                          // if ( $due >= 0 ) {
                          //   echo "Due : " .$due ;
                          // }else{
                          //     echo "Advance : " .$adv ;
                          // }

                        ?>
                      </td>
                      <td>{{ $item->date }}</td>
                      <td>
                        <a href="{{ URL::to("/sales_edit/".$item->id )}}" class="btn btn-success waves-effect waves-light btn-sm"><i class="fa fa-pencil"></i></a>
                        <a href="{{ URL::to("/sales_delete/".$item->id )}}" class="btn btn-danger waves-effect waves-light btn-sm"><i class="fa fa-trash-o"></i></a>
                      </td>
                    </tr>
                    @endforeach

                  </tbody>
                </table>
               </div>

            </div> <!-- End Card Body -->
          </div>
        </div>

      <div class="col-md-12 mt-3">
        <a href="/sale_search" class="btn btn-outline-secondary m-1" id="printPageButton"><i class="fa fa-search"></i> Search Again</a>
      </div>
    
      </div> <!-- End Row -->

    </div><!-- End container-fluid-->
    
</div> <!-- End Content waper -->
@endsection