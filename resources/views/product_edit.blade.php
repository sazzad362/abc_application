@extends('layouts.template')

@section('content')

<div class="content-wrapper">
    <div class="container-fluid">

        <!--Start Dashboard Content-->
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        @if( session('product_create') )
          <div class="alert alert-success p-4" role="alert">
            {{ Session::get('product_create') }}
          </div>
        @endif
        
         <!-- ==========================
            Add Product Form
        =============================-->

      <div class="row">
        <div class="col-lg-12 mt-3">
           <div class="card">
             <div class="card-body">
               <div class="card-title">Edit Product</div>
               <hr>
                <form method="POST" action="{{ URL::to("/update_product/".$product_edit->id)}}">
                {{ csrf_field() }}
               <div class="form-group">
                <label for="input-1">Product Name</label>
                <input type="text" class="form-control" id="input-1" name="product_name" value="{{ $product_edit->product_name }}" required="TRUE" autocomplete="OFF">
               </div>
               <div class="form-group">
                <label for="input-1">Product Code</label>
                <input type="text" class="form-control" id="input-1" name="product_code" value="{{ $product_edit->product_code}}" required="TRUE" autocomplete="OFF">
               </div>
               <div class="form-group">
                <button type="submit" class="btn btn-primary shadow-primary px-5"><i class="icon-lock"></i> Update</button>
              </div>
              </form>
             </div>
           </div>
        </div> <!-- End Col 12 -->

      </div><!--End Row-->

        

    </div><!-- End container-fluid-->
    
</div> <!-- End Content waper -->
@endsection