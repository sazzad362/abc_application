@extends('layouts.template')

@section('content')

<div class="content-wrapper">
    <div class="container-fluid">

        <!--Start Dashboard Content-->
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        <div class="row mt-4">
            <div class="col-12 col-lg-6 col-xl-4">
              <div class="card bg-dark">
                <div class="card-body">
                  <div class="media">
                  <div class="media-body text-left">
                    <h4 class="text-danger">{{ $customer }}</h4>
                    <span class="text-white">Total Customers</span>
                  </div>
                </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-lg-6 col-xl-4">
              <div class="card bg-dark">
                <div class="card-body">
                  <div class="media">
                  <div class="media-body text-left">
                    <h4 class="text-danger">{{ $product }}</h4>
                    <span class="text-white">Total Products</span>
                  </div>
                </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-lg-6 col-xl-4">
              <div class="card bg-dark">
                <div class="card-body">
                  <div class="media">
                  <div class="media-body text-left">
                    <h4 class="text-danger">
                         <?php 
                            $totalSales = 0;
                            foreach($sales as $sale){
                                $totalSales+= $sale -> sales_amount;
                            }
                            echo $totalSales;
                         ?>
                    </h4>
                    <span class="text-white">Total Sales</span>
                  </div>
                </div>
                </div>
              </div>
            </div>
        </div>

        <!-- ==============
            Data Filter
          =============== -->
    <div class="row">
        <div class="col-lg-12 mt-3">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Search Area</h5>

              <form method="POST" action="{{ URL::to("/customer_search")}}">
                {{ csrf_field() }}
              <div class="row">

              <div class="col-md-12">
                <div class="form-group">
                    <label for="input-3">Select Customer</label>
                    <select class="form-control" id="chosecustomer" name="customer_id" required="">
                      <option>Company List</option>
                      @foreach( $cusInfo as $item )
                    <option value="{{ $item->customner_id }}">{{ $item->name }} ({{ $item->address }})</option>
                     @endforeach

                    </select>
                </div>
              </div>

                  <div class="col-md-12">
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary px-5 btn-block mt-3"><i class="fa fa-search" aria-hidden="true"></i> Search </button>
                    </div>
                  </div>
               </div>
              </form> 
            </div>
          </div>
        </div>
      </div>

        <div class="row">
            <div class="col-lg-12">
               <div class="card">
                  <div class="card-header text-uppercase">About AB Ceramics Industries</div>
                  <div class="card-body">
                     <blockquote class="blockquote">
                      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                      <footer class="blockquote-footer">From Name Goes Here</footer>
                    </blockquote>
                  </div>
                </div>
            </div>
          </div>

    </div><!-- End container-fluid-->
    
</div> <!-- End Content waper -->
@endsection
