@extends('layouts.template')

@section('content')

<div class="content-wrapper">
    <div class="container-fluid">

        <!--Start Dashboard Content-->
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        @if( session('create_payment') )
          <div class="alert alert-success p-4" role="alert">
            {{ Session::get('create_payment') }}
          </div>
        @endif
      	<!-- ==============
            Data Filter
          =============== -->
    <div class="row">
        <div class="col-lg-12 mt-3">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Customer Ladger / Statement</h5>

              <form method="POST" action="{{ URL::to("/search_customer_statement")}}">
                {{ csrf_field() }}
              <div class="row">

              <div class="col-md-4">
                <div class="form-group">
                    <label for="input-3">Select Customer</label>
                    <select class="form-control" id="chosecustomer" name="customer_id" required="">
                      <option>Company List</option>
                      @foreach( $customer as $item )
                    <option value="{{ $item->customner_id }}">{{ $item->name }} ({{ $item->address }})</option>
                     @endforeach

                    </select>
                </div>
              </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="input-3">Starting Date</label>
                      <input type="date" class="form-control" id="input-3" name="start_date" placeholder="Select Date" required="TRUE" autocomplete="OFF">
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="input-3">End Date</label>
                      <input type="date" class="form-control" id="input-3" name="end_date" placeholder="Select Date" required="TRUE" autocomplete="OFF">
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary px-5 btn-block mt-3"><i class="fa fa-search" aria-hidden="true"></i> Search </button>
                    </div>
                  </div>
               </div>
              </form> 
            </div>
          </div>
        </div>
      </div>

    </div><!-- End container-fluid-->
    
</div> <!-- End Content waper -->
@endsection