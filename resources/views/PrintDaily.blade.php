<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Print Data</title>
  <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet"/>
  <link href="{{ asset('assets/css/icons.css') }}" rel="stylesheet" type="text/css"/>
  <style>
    .print_margin{
      margin-top: 100px;
    }
    .table td, .table th {
      padding: 7px 20px;
      vertical-align: top;
      border-top: 1px solid #dee2e6;
  }
  .header_title h5 {
    padding: 10px;
    background: #ddd;
    color: #333;
    text-align: center;
}
@media print {
  #printPageButton {
    display: none;
  }
}
  </style>
</head>
<body>

  <div class="container print_margin">
    <div class="header_title">
      <h5>Ladger Sheet For : <span class="text-danger"><?php echo date("d-m-Y") ?> </span></h5>

    </div>
  <table class="table_area table table-bordered">
    <thead class="table_head">
      <!-- =======================
              Table Header ======= -->
      <tr>
        <th scope="col">SL NO</th>
        <th scope="col">Customer Name</th>
        <th scope="col">Pre.Due</th>
        <th scope="col">Payment</th>
        <th scope="col">Reset Amount</th>
        <th scope="col">Sales</th>
        <th scope="col">Due</th>
        <th scope="col">Advance</th>
      </tr>
    </thead>
    <tbody class="table_body">
      <!-- ================
            Table Content = -->
      <?php $i = 1; ?>
     @foreach( $DailyLadger as $item )
      <?php $i++; ?>
        <tr>
          <td scope="row">{{ $item->id }}</td>
          <td>{{ $item->name }} ({{ $item->address }}) </td>
          <td>
            {{ abs($item->pre_due_amount) }}
          </td>
          <td>{{ $item->paid_amount }}</td>
          <td>
            <?php $reset_amount = $item->paid_amount - abs($item->pre_due_amount)  ?>
              {{ abs($reset_amount) }}
          </td>

          <td>{{ $item->sales_amount }}</td>

          <?php $NewBlance = $item->sales_amount - $reset_amount ?> 

          <td class="text-danger">

            <?php 
              if ($NewBlance > 0) {
                echo abs($NewBlance);
              }else{
                echo "N/A";
              }
             ?>

          </td>

          <td>
            <?php 
              if ($NewBlance < 0) {
                echo abs($NewBlance);
              }else{
                echo "N/A";
              }
            ?>
          </td>

        </tr>
      @endforeach

    </tbody>
</table>


<div class="btn-group" role="group" aria-label="Basic example">
 <a href="javascript:window.print();" target="_blank" class="btn btn-outline-secondary m-1" id="printPageButton"><i class="fa fa-print"></i> Print</a>
  <a href="/home" class="btn btn-outline-secondary m-1" id="printPageButton"><i class="fa fa-home"></i> Go Back</a>
</div>

</div>

</body>
</html>