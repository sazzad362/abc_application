<?php  

 foreach ($invoices_customer as $item) {

   $customer_id = $item->id;

   $customer_name = $item->name;

   $billing_address = $item->billing_address;

   $billing_location = $item->billing_location;

   $billing_phone = $item->billing_phone;

   $sub_total = $item->sub_total;

   $fare = $item->fare;

   $intensive = $item->intensive;

   $paid = $item->paid;

   $grand_total = $item->grand_total;

   $due = $item->due;

  $pre_deu = $item->pre_deu;



   $shipping_address = $item->shipping_address;

   $shipping_location = $item->shipping_location;

   $shipping_phone = $item->shipping_phone;

   $created_at = $item->created_at;

   $order_id = $item->order_id;

 }

?>

<!DOCTYPE html>

<html lang="en">

<head>

  <meta charset="UTF-8">

  <title>ABC/<?php echo date("Y"); ?>/{{ $order_id }}</title>

  <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet"/>

  <link href="{{ asset('assets/css/icons.css') }}" rel="stylesheet" type="text/css"/>

  <style>

    .card{

      border:0;

      border-radius: 0;

    }

    .table td, .table th {

          padding: 7px 20px;

          vertical-align: top;

          border-top: 1px solid #dee2e6;

      }

  

    .payment-left {

        width: 650px;

        margin-right: 38px;

        margin-left: 15px;

    }



    .payment-right {

        width: 381px;

    }



    .text-muted {

        color: #6c757d!important;

        font-size: 14px;

        text-align: center;

    }



    .payment-left{

      position: relative;

    }

    .auth_sign {

        position: absolute;

        bottom: 0;

    }



    .invoice_head img{

      width: 100%;

      margin-bottom: 30px;

    }

    .two {
    left: 30%;
}

  .three {
    left: 60%;
}
td{
  text-align: center;
}
    @media print {

      #printPageButton {

        display: none;

      }

      .payment-left {

        width: 505px;

        margin-right: 38px;

      }

    }

  </style>

  

</head>



<body>

<!-- Start wrapper-->

 <div id="wrapper">

  <div class="content-wrapper m-0">

    <div class="container">

      <div class="card">

          <div class="card-body">

         <!-- Main content -->

                  <div class="invoice_head">

                    <img src="{{ URL::to('assets/images/abc-pad.png')}}" alt="">

                  </div>

                  <section class="invoice">

                    <!-- title row -->    

                    <div class="row invoice-info">

                      <div class="col-sm-4 invoice-col">

                        Bill To

                        <address>

                         <strong>{{ $billing_address }}</strong><br>

                          {{ $billing_location }}<br>

                          Phone: {{ $billing_phone }}<br>

                        </address>

                      </div><!-- /.col -->

                      <div class="col-sm-4 invoice-col">

                        Ship To

                        <address>

                          <strong>{{ $shipping_address }}</strong><br>

                          {{ $shipping_location }}<br>

                          Phone: {{ $shipping_phone }}<br>

                        </address>

                      </div><!-- /.col -->

                      <div class="col-sm-4 invoice-col text-right">

                        <b>Invoice ID:</b> ABC/<?php echo date("Y"); ?>/{{ $order_id }}<br>

                        <b>Date:</b>  <?php echo $newStartDate = date("d-m-Y", strtotime($created_at)); ?><br>

                      </div><!-- /.col -->

                    </div><!-- /.row -->



                    <!-- Table row -->

                    <div class="row">

                      <div class="col-12">

                        <table class="table table-bordered">

                          <thead>

                            <tr>

                              <th>Product Name</th>

                              <th>Code</th>

                              <th>Grade</th>

                              <th>Specification</th>

                              <th>Ctn./Pcs</th>

                              <th>Qty./Sft</th>

                              <th> Quantity </th>

                              <th>Rate</th>

                              <th>Price</th>

                            </tr>

                          </thead>

                          <tbody>

                            <?php  

                                $TotalCTN = 0;
                                $TotalQuantity = 0;

                                foreach ($invoices_customer as $item) {

                                $TotalCTN += $item->product_ctn;


                                $Quantity = $item->product_ctn * $item->product_qty;

                                $TotalQuantity += $Quantity;

                            ?>

                            <tr>

                              <td>{{ $item->product_name }}</td>

                              <td>{{ $item->product_code }}</td>

                              <td>{{ $item->product_grade }}</td>

                              <td>{{ $item->product_specification }}</td>

                              <td>{{ $item->product_ctn }}</td>

                              <td>{{ $item->product_qty }}</td>

                              <td>{{ $Quantity }}</td>

                              <td>{{ $item->product_rate }}</td>

                              <td>{{ $item->product_ctn * $item->product_qty * $item->product_rate }}</td>

                            </tr>

                            <?php } ?>

                            <tr>
                              <td class="text-center">Total: </td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td class="text-center">
                               {{ $TotalCTN }}
                              </td>
                              <td></td>
                              <td class="text-center">
                                {{ $TotalQuantity }}
                              </td>
                              <td></td>
                              <td class="text-center">{{ $sub_total }}</td>
                            </tr>

                          </tbody>

                        </table>

                      </div><!-- /.col -->

                    </div><!-- /.row -->



                    <div class="row left_right">

                      <!-- accepted payments column -->

                      <div class="payment-left">

                        <p class="text-muted bg-light p-2 mt-3 border rounded">

                          Monthly Commission has been adjusted in the Rate of the invoice/Cheque Particulars(If any).

                        </p>

                        <div class="auth_sign one text-center">
                          <br>
                        ----------------------
                          <p>Sr. Executive</p>
                        </div>

                        <div class="auth_sign two text-center">
                          <br>
                          ---------------------
                          <p>Asst. Manager</p>
                        </div>

                        <div class="auth_sign three text-center">
                          <br>
                      -----------------
                          <p>A.G.M</p>
                        </div>

                      </div><!-- /.col -->

                      <div class="payment-right mt-3">

                        <div class="table-sub">

                          <table class="table table-bordered">



                            <tbody>



                             <tr>



                              <th style="width:50%">Subtotal:</th>



                              <td>{{ $sub_total }}</td>



                            </tr>



                            <tr>



                              <th>Pre. Due:</th>



                              <td>{{ $pre_deu }}</td>



                            </tr>



                            <tr>



                              <th>Grand Total:</th>



                              <td>{{ $grand_total }}</td>



                            </tr>



                            <tr>



                              <th>Fare:</th>



                              <td>{{ $fare }}</td>



                            </tr>



                            <tr>



                              <th>Incentive:</th>



                              <td>{{ $intensive }}</td>



                            </tr>



                            <tr>



                              <th>Paid:</th>



                              <td>{{ $paid }}</td>



                            </tr>



                            <tr>



                              <th>Due / Adv:</th>



                              <td>{{ $due }}</td>



                            </tr>



                          </tbody>



              </table>

                        </div>

                      </div><!-- /.col -->

                    </div><!-- /.row -->



                    <!-- this row will not appear when printing -->



                    <div class="row no-print">

                      <div class="col-lg-3">

                        <a href="javascript:window.print();" id="printPageButton" class="btn btn-outline-secondary m-1"><i class="fa fa-print"></i> Print</a>

            </div>

                    </div>

                  </section><!-- /.content -->

          </div>

      </div>



    </div>

    <!-- End container-fluid-->

    

   </div><!--End content-wrapper-->



   

  </div><!--End wrapper-->



</body>

</html>

