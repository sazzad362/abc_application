@extends('layouts.template')

@section('content')

<div class="content-wrapper">
    <div class="container-fluid">

        <!--Start Dashboard Content-->
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        @if( session('customer_create') )
          <div class="alert alert-success p-4" role="alert">
            {{ Session::get('customer_create') }}
          </div>
        @endif

        <!-- ==========================
            Add New Customet Form
        =============================-->

      <div class="row">
        <div class="col-lg-12">
           <div class="card">
             <div class="card-body">
               <div class="card-title">Add New Customer</div>
               <hr>
                <form method="POST" action="{{ URL::to("/update_customer/".$customer_edit->id)}}">
                {{ csrf_field() }}
               <div class="form-group">
                <label for="input-1">Name</label>
                <input type="text" class="form-control" id="input-1" name="company_name" value="{{ $customer_edit->name }}" required="TRUE" autocomplete="OFF">
               </div>
               <div class="form-group">
                <label for="input-2">Email</label>
                <input type="text" class="form-control" id="input-2" name="email" value="{{ $customer_edit->email }}">
               </div>
               <div class="form-group">
                <label for="input-3">Phone</label>
                <input type="text" class="form-control" id="input-3" name="phone" value="{{ $customer_edit->phone }}" required="TRUE" autocomplete="OFF">
               </div>
               <div class="form-group">
                <label for="input-3">Address</label>
                <input type="text" class="form-control" id="input-3" name="address" value="{{ $customer_edit->address }}" required="TRUE" autocomplete="OFF">
               </div>
               <div class="form-group">
                <button type="submit" class="btn btn-primary shadow-primary px-5"><i class="icon-lock"></i> Submit</button>
              </div>
              </form>
             </div>
           </div>
        </div>
      </div><!--End Row-->

    </div><!-- End container-fluid-->
    
</div> <!-- End Content waper -->
@endsection