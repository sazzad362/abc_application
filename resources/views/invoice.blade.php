@extends('layouts.template')

@section('content')

<div class="content-wrapper">
    <div class="container-fluid">

            <!-- ==========================
                  Add New Customet Form
              =============================-->

            <div class="row">
              <div class="col-lg-12">
                
                @if( session('invoice_create') )
                  <div class="alert alert-success p-4" role="alert">
                    {{ Session::get('invoice_create') }}
                  </div>
                @endif

                 <div class="card sd-invoice">
                   <div class="card-body">
                     <div class="card-title">Genarate New Invoice</div>
                     <hr>
                      <form method="POST" action="{{ URL::to("/add_invoice/")}}">
                        {{ csrf_field() }}
                        
                        <input id="_token" type="hidden" value="{{ csrf_token() }}">

                      <div class="form-group">
                        <label for="chosecustomer">Chose Customer</label>
                        <select name="customer_id" class="form-control" id="chosecustomer" required="">
                          <option value="">Company List</option>
                          @foreach ($customer as $item)
                            <option value="{{ $item->customner_id }}">{{ $item->name }} ({{ $item->address }})</option>
                          @endforeach
                        </select>
                        <script>
                          $(document).ready(function(){ 
                              $("#chosecustomer").change(function(){
                                var customer_id = $(this).val();
                                var _token = $('#_token').val();
                                $.ajax({
                                    type:'POST',
                                    url:"/load-data",
                                    data:{
                                      customer_id: customer_id,
                                      _token: "{{ csrf_token() }}"
                                    },
                                    success: function( data ) {
                                      $('#billing_address').val(data[0].name);
                                      $('#billing_location').val(data[0].address);
                                      $('#billing_phone').val(data[0].phone);
                                    }
                                });
                                $.ajax({
                                    type:'POST',
                                    url:"/load-due",
                                    data:{
                                      customer_id: customer_id,
                                      _token: "{{ csrf_token() }}"
                                    },
                                    success: function( data ) {

                                      // console.log(data);
                                      $('#pre_due').val(data);
                                    }
                                });
                              })
                          });  
                        </script>
                      </div>
                     
                      <div class="row">
                        <div class="col-md-12">
                          <label>Billing Address</label>
                        </div>
                         <div class="col-md-4">
                           <div class="form-group">
                            <input name="billing_address" type="text" class="form-control" id="billing_address" placeholder="Address Name" required="" autocomplete="OFF">
                           </div>
                         </div>
                         <div class="col-md-4">
                           <div class="form-group">
                            <input name="billing_location" type="text" class="form-control" id="billing_location" placeholder="Location. EX: Shamoly, Dhaka" required="" autocomplete="OFF">
                           </div>
                         </div>
                         <div class="col-md-4">
                           <div class="form-group">
                            <input name="billing_phone" type="text" class="form-control" id="billing_phone" placeholder="Phone Number" required="" autocomplete="OFF">
                           </div>
                         </div>
                      </div> <!-- End Billing Address -->

                     <div class="row">
                      <div class="col-md-12">
                        <label>Shipping Address</label>
                      </div>
                       <div class="col-md-4">
                         <div class="form-group">
                          <input name="shipping_address" type="text" class="form-control" id="input-2" placeholder="Address Name" required="" autocomplete="OFF">
                         </div>
                       </div>
                       <div class="col-md-4">
                         <div class="form-group">
                          <input name="shipping_location" type="text" class="form-control" id="input-2" placeholder="Location. EX: Shamoly, Dhaka" required="" autocomplete="OFF">
                         </div>
                       </div>
                       <div class="col-md-4">
                         <div class="form-group">
                          <input name="shipping_phone" type="text" class="form-control" id="input-2" placeholder="Phone Number" required="" autocomplete="OFF">
                         </div>
                       </div>
                     </div> <!-- End Shipping Address -->
                      
                    <!-- ====================
                          Invoice Data ====== -->

                    <div class="table-responsive">
                      <table class="table table-bordered text-center sd-invoice-1" id="tab_logic">
                        <thead>
                          <tr>
                            <th scope="col">Product Name</th>
                            <th scope="col" width="150">Code</th>
                            <th scope="col" width="100">Grade</th>
                            <th scope="col">Specification</th>
                            <th scope="col">Ctn./Pcs</th>
                            <th scope="col">Qty.Sft.</th>
                            <th scope="col">Rate</th>
                            <th scope="col">Price</th>
                          </tr>
                        </thead>
                        <tbody>

                          <tr id='addr0'>
                            <td scope="row">
                              <select name="product_name[]" class="form-control">
                                <option>Product Name</option>
                                @foreach ($product as $item)
                                  <option value="{{ $item->product_name }}">{{ $item->product_name }}</option>
                                @endforeach
                              </select>
                            </td>
                            <td>
                              <select name="product_code[]" class="form-control">
                                <option>Code</option>
                                @foreach ($product as $item)
                                  <option value="{{ $item->product_code }}">{{ $item->product_code }}</option>
                                @endforeach
                              </select>
                            </td>
                            <td>
                              <select name="product_grade[]" class="form-control">
                                @foreach ($grade as $item)
                                  <option value="{{ $item->grade_name }}">{{ $item->grade_name }}</option>
                                @endforeach
                              </select>
                            </td>
                            <td>
                              <select name="product_specification[]" class="form-control">
                                @foreach ($specification as $item)
                                  <option value="{{ $item->specification_name }}">{{ $item->specification_name }}</option>
                                @endforeach
                              </select>
                            </td>
                            <td>
                              <input name="product_ctn[]" type="text" class="form-control ctn" placeholder="100" autocomplete="OFF">
                            </td>
                            <td>
                              <input name="product_qty[]" type="text" class="form-control qty" placeholder="17.777" autocomplete="OFF">
                            </td>
                            <td>                        
                              <input name="product_rate[]" type="text" class="form-control rate" placeholder="Rate" autocomplete="OFF">
                            </td>
                            <td>                        
                              <input type="text" class="form-control price" placeholder="0" autocomplete="OFF" readonly="">
                            </td>
                          </tr>

                          <tr id='addr1'></tr>

                        </tbody>
                      </table>
                     </div>
                      
                    <div class="form-group mt-2">
                     <button type="button" class="btn btn-success btn-sm waves-effect waves-light" id="add_row">Add Row</button>
                     <button type="button" class="btn btn-danger btn-sm waves-effect waves-light" id="delete_row">Remove Row</button>
                    </div>
                    
                    <!-- ===================
                          Calculation Func
                    ========================= -->

                    <div class="row">
                      <div class="offset-md-8 col-md-4">
                        <div class="block sd-center">
                          <table class="table table-bordered table-hover" id="tab_logic">
                            <tbody>
                              <tr>
                                <th class="text-center">Sub Total</th>
                                <td class="text-center"><input type="number" name="sub_total" placeholder="0.00" class="form-control subtotal" id="sub_total" readonly=""></td>
                              </tr>
                              <tr>
                                <th class="text-center">Pre. Due</th>
                                <td class="text-center"><input type="number" name="pre_due" class="form-control pre_due" id="pre_due" readonly=""></td>
                              </tr>
                              <tr>
                                <th class="text-center">Grand Total</th>
                                <td class="text-center"><input name="grand_total" type="text" class="form-control grandTotal" placeholder="0" readonly=""></td>
                              </tr>
                              <tr>
                                <th class="text-center">Fare</th>
                                <td class="text-center"><div class="input-group mb-2 mb-sm-0">
                                    <input name="fare" type="text" class="form-control" id="fare" placeholder="Rate Fee">
                                  </div></td>
                              </tr>
                              <tr>
                                <th class="text-center">Incentive</th>
                                <td class="text-center"><div class="input-group mb-2 mb-sm-0">
                                    <input name="intensive" type="text" class="form-control" id="intensive" placeholder="Incentive Amount">
                                  </div></td>
                              </tr>
                              <tr>
                                <th class="text-center">Paid</th>
                                <td class="text-center"><div class="input-group mb-2 mb-sm-0">
                                    <input name="paid" type="text" class="form-control" id="paid" placeholder="Paid Amount">
                                  </div></td>
                              </tr>
                              <tr>
                                <th class="text-center">Due / Adv.</th>
                                <td class="text-center"><input name="due" type="text" class="form-control due" placeholder="0" readonly=""></td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>

                                   
                     <div class="form-group mt-5">
                      <button type="submit" class="btn btn-primary shadow-primary px-5 float-right"><i class="icon-lock"></i> Save Invoice</button>
                    </div>
                    </form>
                   </div> <!-- Card Body -->
                 </div>
              </div>
            </div><!--End Row-->



               <!--Start Back To Top Button-->
                <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>
                <!--End Back To Top Button-->
              
              <!--Start footer-->
              <footer class="footer">
                  <div class="container">
                    <div class="text-center">
                      Copyright © 2018 <a href="https://smarterdevs.com">Smarterdevs LLC</a> | Simple, Modern, Trendy
                    </div>
                  </div>
                </footer>
              <!--End footer-->

    </div><!-- End container-fluid-->
    
</div> <!-- End Content waper -->
@endsection