@extends('layouts.template')

@section('content')

<div class="content-wrapper">
    <div class="container-fluid">

        <!--Start Dashboard Content-->
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        @if( session('customer_delete') )
          <div class="alert alert-success p-4" role="alert">
            {{ Session::get('customer_delete') }}
          </div>
        @endif

        @if( session('customer_update') )
          <div class="alert alert-success p-4" role="alert">
            {{ Session::get('customer_update') }}
          </div>
        @endif

        <!-- ==========================
            All Customer Customet Form
        =============================-->
      
      <div class="row">
        <div class="col-lg-12 mt-3">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Customer List</h5>
               <div class="table-responsive">
                <table class="table table-bordered text-center">
                  <thead>
                    <tr>
                      <th scope="col">SL NO</th>
                      <th scope="col">Name</th>
                      <th scope="col">Address</th>
                      <th scope="col">Phone</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                      
                    <?php 
                       $i = 1;
                     ?>

                     @foreach( $customer as $item )

                    <tr>
                      <th scope="row"> <?php echo $i++; ?> </th>
                      <td>{{ $item->name }}</td>
                      <td>{{ $item->address }}</td>
                      <td>{{ $item->phone }}</td>
                      <td>
                        <a href="{{ URL::to("/customer_edit/".$item->id )}}" class="btn btn-success waves-effect waves-light btn-sm"><i class="fa fa-pencil"></i></a>
                        <a href="{{ URL::to("/customer_delete/".$item->id )}}" class="btn btn-danger waves-effect waves-light btn-sm"><i class="fa fa-trash-o"></i></a>
                      </td>
                    </tr>
                     @endforeach

                  </tbody>
                </table>
               </div>
            </div>
          </div>
        </div>

        <!-- ===================
              Pagination ======== -->

        <div class="sd-pagination ml-3">
          {{ $customer->links() }}
        </div>

      </div> <!-- End Row -->

    </div><!-- End container-fluid-->
    
</div> <!-- End Content waper -->
@endsection