@extends('layouts.template')

@section('content')

<div class="content-wrapper">
    <div class="container-fluid">

        <!--Start Dashboard Content-->
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        @if( session('create_payment') )
          <div class="alert alert-success p-4" role="alert">
            {{ Session::get('create_payment') }}
          </div>
        @endif

      <!-- ==========================
            Add New Payment
        =============================-->

      <div class="row">
        <div class="col-lg-12 mt-3">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Today's Ledger / Date: <?php echo date("Y-m-d") ?></h5>
               <div class="table-responsive">
                <table class="table table-bordered text-center">
                  <thead>
                    <!-- =======================
                            Table Header ======= -->
                    <tr>
                      <th scope="col">SL NO</th>
                      <th scope="col">Customer Name</th>
                      <th scope="col">Pre.Due</th>
                      <th scope="col">Payment</th>
                      <th scope="col">Reset Amount</th>
                      <th scope="col">Sales</th>
                      <th scope="col">Due</th>
                      <th scope="col">Advance</th>
                    </tr>
                  </thead>
                  <tbody>
                    <!-- ================
                          Table Content = -->
                    <?php $i = 0; ?>
                   @foreach( $DailyLadger as $item )
                    <?php $i++; ?>
                      <tr>
                        <th scope="row">{{ $i }}</th>
                        <th>{{ $item->name }} ({{ $item->address }})</th>
                        <td>
                          <?php 

                            echo $predue =  abs($item->pre_due_amount) ;

                           ?>
                        </td>
                        <td>{{ $item->paid_amount }}</td>
                        <td>
                          <?php $reset_amount = $item->paid_amount - $predue  ?>
                            {{ abs($reset_amount) }}
                        </td>

                        <td>{{ $item->sales_amount }}</td>

                        <?php $NewBlance = $item->sales_amount - $reset_amount ?> 

                        <td class="text-danger">

                          <?php 
                            if ($NewBlance > 0) {
                              echo abs($NewBlance);
                            }else{
                              echo "N/A";
                            }
                           ?>

                        </td>

                        <td>
                          <?php 
                            if ($NewBlance < 0) {
                              echo abs($NewBlance);
                            }else{
                              echo "N/A";
                            }
                          ?>
                        </td>

                      </tr>
                    @endforeach

                  </tbody>
                </table>
               </div>

               <!-- =============
                     Print Button =====-->
              <div class="print-btn mt-4">
                <a href="/print_daily" class="btn btn-outline-secondary m-1"><i class="fa fa-print"></i> Print</a>
              </div>

            </div> <!-- End Card Body -->
          </div>
        </div>

      </div> <!-- End Row -->

    </div><!-- End container-fluid-->
    
</div> <!-- End Content waper -->
@endsection