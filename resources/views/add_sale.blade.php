@extends('layouts.template')

@section('content')

<div class="content-wrapper">
    <div class="container-fluid">

        <!--Start Dashboard Content-->
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        @if( session('sales_create') )
          <div class="alert alert-success p-4" role="alert">
            {{ Session::get('sales_create') }}
          </div>
        @endif

        <!-- ==========================
            Add New Sales
        =============================-->

      <div class="row">
        <div class="col-lg-12">
           <div class="card">
             <div class="card-body">
               <div class="card-title">Add Sales</div>
               <hr>
                <form method="POST" action="{{ URL::to("/create_sales")}}">
                {{ csrf_field() }}
                <div class="form-group">
                  <label for="chosecustomer">Chose Customer</label>
                  <select class="form-control" id="chosecustomer" name="customer_id" required="">
                    <option readonly>Company List</option>

                    @foreach( $customer as $item )
                    <option value="{{ $item->customner_id }}">{{ $item->name }} ({{ $item->address }})</option>
                     @endforeach

                  </select>
                </div>
               <div class="form-group">
                <label for="input-2">Sale Amount</label>
                <input type="text" class="form-control" id="sale_saleId" name="sales" placeholder="Enter Sales Amount" required="" autocomplete="OFF">
               </div>
               <div class="form-group">
                <label for="input-3">Paid Amount</label>
                <input type="text" class="form-control" id="sale_paidId" name="paid" placeholder="Enter Paid Amount" required="TRUE" autocomplete="OFF">
               </div>
               <div class="form-group">
                <label for="input-3">Due Amount</label>
                <input type="text" class="form-control" id="sales_due" name="due" value="">
               </div>
               <div class="form-group">
                <label for="input-5">Sales Ctn.</label>
                <input type="text" class="form-control" id="sale_ctn" name="sales_ctn" value="0" required="" autocomplete="OFF">
               </div>
               <div class="form-group">
                <label for="input-3">Date</label>
                <input type="date" class="form-control" id="input-3" name="date" placeholder="Select Date" required="TRUE" autocomplete="OFF">
               </div>
               <div class="form-group">
                <button type="submit" class="btn btn-primary shadow-primary px-5"><i class="icon-lock"></i> Submit</button>
              </div>
              </form>
             </div>
           </div>
        </div>
      </div><!--End Row-->

    </div><!-- End container-fluid-->
    
</div> <!-- End Content waper -->
@endsection