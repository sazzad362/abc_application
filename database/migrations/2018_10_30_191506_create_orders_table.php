<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('billing_address');
            $table->string('billing_location');
            $table->string('billing_phone');
            $table->string('shipping_address');
            $table->string('shipping_location');
            $table->string('shipping_phone');
            $table->string('sub_total');
            $table->string('fare')->nullable();;
            $table->string('intensive')->nullable();;
            $table->string('paid');
            $table->string('grand_total');
            $table->string('due');
            $table->unsignedInteger('customner_id');
            $table->string('pre_deu')->nullable();
            $table->foreign('customner_id')->references('customner_id')->on('customers')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
