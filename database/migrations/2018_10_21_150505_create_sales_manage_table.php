<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesManageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_manage', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sales_amount');
            $table->string('paid_amount');
            $table->string('pre_due_amount')->nullable();
            $table->string('date');
            $table->unsignedInteger('customner_id');
            $table->unsignedInteger('sales_ctn');
            $table->foreign('customner_id')->references('customner_id')->on('customers')->onDelete('cascade');
            $table->timestamps();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_manage');
    }
}
