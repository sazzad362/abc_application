<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GradeModel extends Model
{
    protected $guarded = [];
    protected $table = 'product_grades';
}
