<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SpecificationModel extends Model
{
    protected $guarded = [];
    protected $table = 'specification';
}
