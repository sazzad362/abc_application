<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeuPayment extends Model
{
    protected $guarded = [];
    protected $table = 'due_payments';
}
