<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddCustomerModel extends Model
{
    protected $guarded = [];
    protected $table = 'customers';
}
