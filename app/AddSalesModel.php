<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddSalesModel extends Model
{
    protected $guarded = [];
    protected $table = 'sales_manage';
}
