<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\AddCustomerModel;

use Session;

class AddCustomerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }
        
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('add_customer');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
    	// Get data form customer table
    	$result = AddCustomerModel::orderBy('id', 'DESC')->first();

    	if ($result) {
			$customerId =  $result->id;
			$NextCustomerId = $customerId + 11 ;
    	}else{
    		$NextCustomerId = 1 ;
    	}

        $Customer = new AddCustomerModel;
        $Customer->name = $request->input('company_name');
        $Customer->customner_id = $NextCustomerId;
        $Customer->email = $request->input('email');
        $Customer->phone = $request->input('phone');
        $Customer->address = $request->input('address');
        $Customer->save();

         Session::flash('customer_create', 'Customer Created Successfully');

        return redirect('/add_customer');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('customer_list');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
