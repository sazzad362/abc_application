<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;



use App\AddCustomerModel;

use App\ProductModel;

use App\GradeModel;

use App\SpecificationModel;

use App\Orders;

use App\Orders_items;



use Session;



class InvoiceListController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {

        $invoice_list = AddCustomerModel::join('orders', 'customers.customner_id', '=', 'orders.customner_id')

                    ->select('*')

                    ->orderBy('orders.id', 'DESC')

                    ->get();



        return view('invoice_list', compact('invoice_list'));

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        //

    }



    /**

     * Show the form for invoice_prient a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function invoice_prient($customer_id)

    {

        $invoices_customer = AddCustomerModel::join('orders', 'customers.customner_id', '=', 'orders.customner_id')

                    ->join('orders_items', 'orders.id', '=', 'orders_items.order_id')

                    ->select('*')

                    ->where('orders.id', '=', $customer_id )

                    ->get();

        

        return view('invoice-print', compact('invoices_customer'));

    }

    // PDF

    public function invoice_pdf($customer_id)

    {

        $invoices_customer = AddCustomerModel::join('orders', 'customers.customner_id', '=', 'orders.customner_id')

                    ->join('orders_items', 'orders.id', '=', 'orders_items.order_id')

                    ->select('*')

                    ->where('orders.id', '=', $customer_id )

                    ->get();

        

        return view('invoice-pdf', compact('invoices_customer'));

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

        //

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        //

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

        //

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)

    {

        //

    }

}

