<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\AddCustomerModel;

use App\ProductModel;

use App\GradeModel;

use App\SpecificationModel;

use App\Orders;

use App\Orders_items;



use Session;

class InvoiceSearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $customer = AddCustomerModel::orderBy('id', 'ASC')->get(); 
        return view('invoice_search', compact('customer'));
    }

    /**
     * Show the data for customer inovice search.
     *
     * @return \Illuminate\Http\Response
     */

   public function search(Request $request)
    {
        $date_start = $request->input('start_date');
        $date_end = $request->input('end_date');
        $cId = $request->input('customer_id');

        $invoice_list = AddCustomerModel::join('orders', 'customers.customner_id', '=', 'orders.customner_id')

                    ->select('*')

                    ->where('orders.customner_id', '=', $cId )

                    ->whereBetween('orders.created_at', [$date_start, $date_end])

                    ->orderBy('orders.id', 'DESC')

                    ->get();

        return view('invoice_search_result', compact('invoice_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
