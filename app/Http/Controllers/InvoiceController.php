<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AddCustomerModel;
use App\ProductModel;
use App\AddSalesModel;
use App\GradeModel;
use App\SpecificationModel;
use App\Orders;
use App\Orders_items;
use App\DeuPayment;
use Session;

class InvoiceController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customer = AddCustomerModel::all();
        $product = ProductModel::all();
        $grade = GradeModel::all();
        $specification = SpecificationModel::all();

        return view('invoice', compact('customer', 'product', 'grade', 'specification'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $orders = new Orders;
        $orders->customner_id = $request->input('customer_id');
        $orders->billing_address = $request->input('billing_address');
        $orders->billing_location = $request->input('billing_location');
        $orders->billing_phone = $request->input('billing_phone');
        $orders->shipping_address = $request->input('shipping_address');
        $orders->shipping_location = $request->input('shipping_location');
        $orders->shipping_phone = $request->input('shipping_phone');
        $orders->sub_total = $request->input('sub_total');
        $orders->fare = $request->input('fare');
        $orders->intensive = $request->input('intensive');
        $orders->paid = $request->input('paid');
        $orders->grand_total = $request->input('grand_total');
        $orders->due = $request->input('due');
        $orders->pre_deu = $request->input('pre_due');
        $get_order = $orders->save();

        if ($orders->id != 0) {
            foreach ($request->product_name as $item => $v) {
                $date = array(
                    "product_name" => $request->input('product_name') [$item],
                    "product_code" => $request->input('product_code') [$item],
                    "product_grade" => $request->input('product_grade') [$item],
                    "product_specification" => $request->input('product_specification') [$item],
                    "product_ctn" => $request->input('product_ctn') [$item],
                    "product_qty" => $request->input('product_qty') [$item],
                    "product_rate" => $request->input('product_rate') [$item],
                    "order_id" => $orders->id,
                    "created_at" => date("y-m-d G.i:s", time()),
                    "updated_at" => date("y-m-d G.i:s", time())
                );
                Orders_items::insert($date);
            }
        }
        Session::flash('invoice_create', 'Invoice Created Successfully');

        return redirect('/invoice');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
       $customerId = $request->input('customer_id');
       if (isset($customerId)) {
           $customer = AddCustomerModel::where('customner_id', $customerId)->get();
           return $customer;

       }
    }
    public function due(Request $request)
    {
       $customerId = $request->input('customer_id');
       if (isset($customerId)) {
           // $sales_manage = AddSalesModel::where('customner_id', $customerId)->orderBy('id', 'DESC')->first();
           // return $sales_manage;

        $dues = DeuPayment::where('customer_id', '=', $customerId)->get();
        $NewPreDue = 0;
            foreach ($dues as $dueItem) {
            $NewPreDue += $dueItem->due_payment;
        }
         return $NewPreDue;

       }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $invoice_delete = Orders::find($id);
        $invoice_delete->delete();

        // Delete order items related to order id
        $order_items = Orders_items::where('order_id', '=', $id);
        $order_items->delete();

        Session::flash('invoice_delete', 'Invoice Deleted');

        return redirect('/invoice_list');
    }
}
