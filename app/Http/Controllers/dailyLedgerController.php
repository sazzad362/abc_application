<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AddCustomerModel;
use App\DeuPayment;

class dailyLedgerController extends Controller
{
    /**
 * Create a new controller instance.
 *
 * @return void
*/
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $todayDate = date("Y-m-d");
        $DailyLadger = AddCustomerModel::join('sales_manage', 'customers.customner_id', '=', 'sales_manage.customner_id')
                    ->select('customers.name','customers.address', 'sales_manage.*')
                    ->where('sales_manage.date', '=', $todayDate )
                    ->orderBy('sales_manage.id', 'ASC')
                    ->get();
        return view('ledger_daily', compact('DailyLadger'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function print()
    {
        $todayDate = date("Y-m-d");
        $DailyLadger = AddCustomerModel::join('sales_manage', 'customers.customner_id', '=', 'sales_manage.customner_id')
                    ->select('customers.name','customers.address', 'sales_manage.*')
                    ->where('sales_manage.date', '=', $todayDate )
                    ->orderBy('sales_manage.id', 'ASC')
                    ->get();
        return view('PrintDaily', compact('DailyLadger'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
