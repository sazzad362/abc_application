<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\GradeModel;

use Session;

class GradeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grade = GradeModel::orderBy('id', 'ASC')->paginate(10);
        return view('grade', compact('grade'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $grade = new GradeModel;
        $grade->grade_name = $request->input('grade_name');
        $grade->save();

        Session::flash('grade_create', 'Grade Created Successfully');

        return redirect('/grade');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $grade_edit = GradeModel::find($id);
        return view('grade_edit', compact('grade_edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $grade_update = GradeModel::find($id);
        $grade_update->grade_name = $request->input('grade_name');
        $grade_update->save();

        Session::flash('grade_update', 'Grade Update Successfully');
        return redirect('/grade');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $grade = GradeModel::find($id);
        $grade->delete();

        Session::flash('grade_delete', 'Grade Deleted');
        return redirect('/grade');
    }
}
