<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\AddCustomerModel;
use App\AddSalesModel;
use App\DeuPayment;
use Session;

class AddSalesController extends Controller
{

/**
 * Create a new controller instance.
 *
 * @return void
*/
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $customer = AddCustomerModel::orderBy('id', 'ASC')->get();        
        return view('add_sale', compact('customer'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $dues = DeuPayment::where('customer_id', '=', $request->input('customer_id'))->get();
        $due_total = 0;
        foreach($dues as $due){
            $due_total+= $due -> due_payment;
        }

        $sales = new AddSalesModel;
        $sales->sales_amount = $request->input('sales');
        $sales->paid_amount = $request->input('paid');
        $sales->date = $request->input('date');
        $sales->customner_id = $request->input('customer_id');
        $sales->sales_ctn = $request->input('sales_ctn');
        $sales->pre_due_amount = $due_total;
        $sales->save();

        $due_payment = new DeuPayment;
        $due_payment->due_payment = $request->input('due');
        $due_payment->customer_id = $request->input('customer_id');
        $due_payment->sales_manage_id = $sales->id;
        $due_payment->save();
        
        Session::flash('sales_create', 'Sales Created Successfully');
        return redirect('/add_sales');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
