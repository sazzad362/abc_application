<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AddCustomerModel;
use App\DeuPayment;

class MonthlyLadgerController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        return view('ledger_monthly');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $date_start = $request->input('start_date');
        $date_end = $request->input('end_date');

        $MonthlyLadger = AddCustomerModel::join('sales_manage', 'customers.customner_id', '=', 'sales_manage.customner_id')
                    ->select('customers.name', 'customers.address', 'sales_manage.*')
                    ->whereBetween('date', [$date_start, $date_end])
                    ->orderBy('sales_manage.id', 'ASC')
                    ->get();
                    
        return view('PrintMonthly', compact('MonthlyLadger', 'date_start', 'date_end'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}