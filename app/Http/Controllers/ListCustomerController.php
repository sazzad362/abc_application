<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use App\AddCustomerModel;

use Session;



class ListCustomerController extends Controller

{

    /**

    * Create a new controller instance.

    *

    * @return void

    */

    public function __construct()

    {

        $this->middleware('auth');

    }

    

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {       

        $customer = AddCustomerModel::orderBy('id', 'DCSE')->paginate(50);

        return view('customer_list', compact('customer'));

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        //

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

        //

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)

    {

        $customer_edit = AddCustomerModel::find($id);

        return view('customer_edit', compact('customer_edit'));

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)

    {

        $customer_update = AddCustomerModel::find($id);

        $customer_update->name = $request->input('company_name');

        $customer_update->email = $request->input('email');

        $customer_update->phone = $request->input('phone');

        $customer_update->address = $request->input('address');

        $customer_update->save();



        Session::flash('customer_update', 'Customer Update Successfully');



        return redirect('/customer_list');

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)

    {

        $customer = AddCustomerModel::find($id);

        $customer->delete();



        Session::flash('customer_delete', 'Customer Deleted');

        return redirect('/customer_list');

    }

}

