<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\AddCustomerModel;
use App\ProductModel;
use App\AddSalesModel;
use App\DeuPayment;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $customer = AddCustomerModel::count();
        $cusInfo = AddCustomerModel::orderBy('id', 'ASC')->get();  
        $product = ProductModel::count();
        $sales = AddSalesModel::orderBy('id', 'ASC')->get();

        return view('home', compact('customer', 'product', 'sales', 'cusInfo'));
    }

    // Search Customer

    public function search(Request $request)
    {
        $cId = $request->input('customer_id');

        $dashboard_search = AddCustomerModel::where('customner_id', '=', $cId)->first();

        // $due_info = AddCustomerModel::join('sales_manage', 'customers.customner_id', '=', 'sales_manage.customner_id')
        //             ->select('customers.name', 'customers.address', 'sales_manage.*')
        //             ->where('customers.customner_id', '=', $cId )
        //             ->orderBy('sales_manage.id', 'DESC')
        //             ->first();

        $due = DeuPayment::where('customer_id', '=', $cId)->get();
        $NewPreDue = 0;
        foreach ($due as $dueItem) {
            $NewPreDue += $dueItem->due_payment;
        }  

        return view('result_customer', compact('dashboard_search', 'NewPreDue'));
    }

}
