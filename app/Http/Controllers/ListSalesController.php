<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\AddCustomerModel;
use App\AddSalesModel;
use App\DeuPayment;
use Session;

class ListSalesController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        // $sales = AddSalesModel::orderBy('id', 'DESC')->get();
        // foreach ($sales as $key) {
        //     $due_payment = DeuPayment::where('sales_manage_id', $key->id)->get();
        //     foreach ($due_payment as $item) {
        //         $db_due_payment = AddSalesModel::find($item->sales_manage_id);
        //         $db_due_payment->pre_due_amount = $item->due_payment;
        //         $db_due_payment->save();
        //     }
        // }

        $sales = AddCustomerModel::join('sales_manage', 'customers.customner_id', '=', 'sales_manage.customner_id')
        ->select('customers.name', 'customers.address', 'sales_manage.*')
        ->orderBy('id', 'DESC')
        ->paginate(50);

        return view('sales_list', compact('sales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sales_edit = AddSalesModel::find($id);
        return view('sales_edit', compact('sales_edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sales = AddSalesModel::find($id);
        $sales->sales_amount = $request->input('sales');
        $sales->paid_amount = $request->input('paid');
        //$sales->due_amount = $request->input('due');
        $sales->date = $request->input('date');
        $sales->customner_id = $request->input('customer_id');
        $sales->sales_ctn = $request->input('sales_ctn');
        $sales->save();

        $due_payment = DeuPayment::find($id);
        $due_payment->due_payment = $request->input('due');
        $due_payment->save();
        
        Session::flash('sales_update', 'Sales Update Successfully');

        return redirect('/all_sales');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sales = AddSalesModel::find($id);
        $sales->delete();

        $due = DeuPayment::where('sales_manage_id', '=', $id);
        $due->delete();

        Session::flash('sales_delete', 'Sales Deleted');
        return redirect('/all_sales');
    }

    /**
     * Search sales
     *
     * @return \Illuminate\Http\Response
     */

    public function searchform()
    {
       return view('sale_search');
    }

    public function search(Request $request)
    {
        $date_start = $request->input('start_date');

        $sales_search = AddCustomerModel::join('sales_manage', 'customers.customner_id', '=', 'sales_manage.customner_id')
        ->select('customers.name', 'customers.address', 'sales_manage.*')
        ->where('sales_manage.date', '=', $date_start )
        ->orderBy('id', 'DESC')
        ->get();

       return view('sale_search_result', compact('sales_search', 'date_start'));
    }
}
