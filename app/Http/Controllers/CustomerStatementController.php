<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AddCustomerModel;
use App\DeuPayment;

class CustomerStatementController extends Controller
{
    /**
 * Create a new controller instance.
 *
 * @return void
*/
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customer = AddCustomerModel::orderBy('id', 'ASC')->get(); 
        return view('customer_statement', compact('customer'));
    }

    /**
     * Show the data for customer statement.
     *
     * @return \Illuminate\Http\Response
     */

   public function search(Request $request)
    {
        $date_start = $request->input('start_date');
        $date_end = $request->input('end_date');
        $cId = $request->input('customer_id');

        $statements = AddCustomerModel::join('sales_manage', 'customers.customner_id', '=', 'sales_manage.customner_id')
                    ->select('customers.name', 'customers.address', 'sales_manage.*')
                    ->where('customers.customner_id', '=', $cId )
                    ->whereBetween('date', [$date_start, $date_end])
                    ->orderBy('sales_manage.id', 'ASC')
                    ->get();
                    
       return view('PrintStatement', compact('statements', 'date_start', 'date_end'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
