<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\SpecificationModel;

use Session;

class SpecificationController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $specification = SpecificationModel::orderBy('id', 'ASC')->paginate(10);
        return view('specification', compact('specification'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $specification = new SpecificationModel;
        $specification->specification_name = $request->input('specification_name');
        $specification->save();

        Session::flash('specification_create', 'Specification Created Successfully');

        return redirect('/specification');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $specification_edit = SpecificationModel::find($id);
        return view('specification_edit', compact('specification_edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $specification_update = SpecificationModel::find($id);
        $specification_update->specification_name = $request->input('specification_name');
        $specification_update->save();

        Session::flash('specification_update', 'Specification Update Successfully');
        return redirect('/specification');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $specification = SpecificationModel::find($id);
        $specification->delete();

        Session::flash('specification_delete', 'Specification Deleted');
        return redirect('/specification');
    }
}
