<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders_items extends Model
{
    protected $guarded = [];
    protected $table = 'orders_items';
}
