<?php



/*

|--------------------------------------------------------------------------

| Web Routes

|--------------------------------------------------------------------------

|

| Here is where you can register web routes for your application. These

| routes are loaded by the RouteServiceProvider within a group which

| contains the "web" middleware group. Now create something great!

|

*/



Route::get('/', function () {

    return view('welcome');

});



Auth::routes();



Route::get('/home', 'HomeController@index')->name('home');



// Route::get('/register', 'HomeController@index')->name('home');



/* =========================

	Route For Customer

============================ */



Route::get('/add_customer', 'AddCustomerController@index')->name('add_customer');

Route::post('/create_customer', 'AddCustomerController@store')->name('create_customer');

Route::get('/customer_list', 'ListCustomerController@index')->name('customer_list');

Route::get('/customer_delete/{id}', 'ListCustomerController@destroy')->name('customer_delete');

Route::get('/customer_edit/{id}', 'ListCustomerController@edit')->name('customer_edit');

Route::post('/update_customer/{id}', 'ListCustomerController@update')->name('update_customer');



/* =========================

	Route For Grade

============================ */



Route::get('/grade', 'GradeController@index')->name('grade');

Route::post('/create_grade', 'GradeController@store')->name('create_grade');

Route::get('/grade_edit/{id}', 'GradeController@edit')->name('grade_edit');

Route::post('/update_grade/{id}', 'GradeController@update')->name('update_grade');

Route::get('/grade_delete/{id}', 'GradeController@destroy')->name('grade_delete');



/* =========================

	Route For Specification

============================ */

Route::get('/specification', 'SpecificationController@index')->name('specification');

Route::post('/create_specification', 'SpecificationController@store')->name('create_specification');

Route::get('/specification_delete/{id}', 'SpecificationController@destroy')->name('specification_delete');

Route::get('/specification_edit/{id}', 'SpecificationController@edit')->name('specification_edit');

Route::post('/update_specification/{id}', 'SpecificationController@update')->name('update_specification');





/* =========================

	Route For Product

============================ */

Route::get('/add_product', 'ProductController@index')->name('product');

Route::post('/create_product', 'ProductController@store')->name('create_product');

Route::get('/all_product', 'ProductViewController@index')->name('all_product');

Route::get('/product_delete/{id}', 'ProductViewController@destroy')->name('product_delete');

Route::get('/product_edit/{id}', 'ProductViewController@edit')->name('product_edit');

Route::post('/update_product/{id}', 'ProductViewController@update')->name('update_product');





/* =========================

	Route For Sales Only

============================ */

Route::get('/add_sales', 'AddSalesController@index')->name('add_sale');

Route::post('/create_sales', 'AddSalesController@store')->name('create_sales');

Route::get('/all_sales', 'ListSalesController@index')->name('all_sales');

Route::get('/sales_delete/{id}', 'ListSalesController@destroy')->name('sales_delete');

Route::get('/sales_edit/{id}', 'ListSalesController@edit')->name('sales_edit');

Route::post('/sales_update/{id}', 'ListSalesController@update')->name('sales_update');

Route::get('/sale_search', 'ListSalesController@searchform')->name('sale_search');

Route::post('/result_sale', 'ListSalesController@search')->name('result_sale');





/* =========================

	Route For Legers

============================ */

Route::get('/ledger_daily', 'dailyLedgerController@index')->name('ledger_daily');

Route::get('/print_daily', 'dailyLedgerController@print')->name('print_daily');



//Monthly

Route::get('/ledger_monthly', 'MonthlyLadgerController@index')->name('ledger_monthly');

Route::post('/search_ledger_monthly', 'MonthlyLadgerController@search')->name('search_ledger_monthly');



// Customer Statements

Route::get('/ledger_customer', 'CustomerStatementController@index')->name('customer_statement');

Route::post('/search_customer_statement', 'CustomerStatementController@search')->name('search_customer_statement');



/* =========================

	Route For Invoice

============================ */

Route::get('/invoice', 'InvoiceController@index')->name('invoice');

Route::post('/add_invoice', 'InvoiceController@store')->name('invoice');

Route::get('/invoice_list', 'InvoiceListController@index')->name('invoice_list');

Route::get('/invoice_prient/{id}', 'InvoiceListController@invoice_prient')->name('invoice_list');

Route::get('/invoice_pdf/{id}', 'InvoiceListController@invoice_pdf')->name('invoice_pdf');

Route::get('/invoice_search', 'InvoiceSearchController@index')->name('invoice_search');

Route::post('/search_customer_invoice', 'InvoiceSearchController@search')->name('search_customer_invoice');

Route::get('/invoice_search_result', 'InvoiceSearchController@search')->name('invoice_search_result');

Route::post('/load-data', 'InvoiceController@show')->name('invoice');

Route::post('/load-due', 'InvoiceController@due')->name('invoice');

Route::get('/invoice_delete/{id}', 'InvoiceController@destroy')->name('invoice_delete');

// Dashboard Search
Route::post('/customer_search', 'HomeController@search')->name('customer_search');

Route::get('/result_customer', 'HomeController@search')->name('result_customer');
